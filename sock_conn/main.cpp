#include <iostream>
#include <boost/asio.hpp>
#include "boost/array.hpp"
#include <string>
#include <vector>

using boost::asio::ip::tcp;

int main()
{
	try
	{
		boost::asio::io_context io_context;
		tcp::resolver resolver(io_context);
		tcp::resolver::results_type endpoints = resolver.resolve("127.0.0.1", "daytime");
		tcp::socket socket(io_context);
		boost::asio::connect(socket, endpoints);

		for (;;)
		{
			boost::array<char, 128> buf;
			boost::system::error_code error;

			size_t len = socket.read_some(boost::asio::buffer(buf), error);
			if (error == boost::asio::error::eof)
			{
				break;
			}
			else if (error)
			{
				break;
			}
			std::cout << buf[0] << std::endl;
		}
	}
	catch (...)
	{
		std::cout << "Error" << std::endl;
	}
	return 0;
}